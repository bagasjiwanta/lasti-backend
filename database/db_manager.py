import sqlite3
import json

class db:
    DB: sqlite3.Connection = None

    def get_db():
        if db.DB is None:
            try: db.DB = sqlite3.connect("database/database.db")
            except sqlite3.Error as e: print(e)
        return db.DB

def read(query, args=(), fields=[]):
    cursor = db.get_db().execute(query, args)
    rows = cursor.fetchall()
    cursor.close()
    return [dict(zip(fields, r)) for r in rows]
