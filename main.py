from fastapi import FastAPI
from services import inventory
from dotenv import load_dotenv

load_dotenv()
app = FastAPI()
app.include_router(inventory.router)