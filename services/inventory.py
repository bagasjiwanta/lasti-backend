from database.db_manager import read
from fastapi import APIRouter, Request

router = APIRouter(prefix="/inventory")

@router.get('/get_by_api_id/{api_id}')
def get_by_api_id(api_id: int, request: Request):
    query = """
        select 
            month || '/' || year as period, visit, share, action
        from 
            history natural inner join api
        where 
            api_id = ?
        order by 
            year asc, month asc      
    """
    rows = read(query, (str(api_id),), ['period', 'visit', 'share', 'action'])
    return { "data" : rows }

@router.get('/get_global_trend')
def get_global_trend(request: Request):
    query = """
        select 
            month || '/' || year as month, sum(visit) as visit, sum(share) as share, sum(action) as action
        from 
            history
        group by 
            year, month
    """
    rows = read(query, fields=['month', 'visit', 'share', 'action'])
    return { "data" : rows }

@router.get('/get_category_stats')
def get_category_stats(request: Request):
    query = """
        select 
            category_name as category, sum(visit) as visit, sum(share) as share, sum(action) as action 
        from 
            history natural inner join api natural inner join category
        where 
            year = '2022' and month = '11'
        group by 
            category_id
    """
    rows = read(query, fields=['category', 'visit', 'share', 'action'])
    return { "data" : rows }

@router.get('/get_api_info')
def get_api_info(request: Request):
    query = """
        select distinct 
            api_name as name, api_author as author, category_name as category
        from 
            category natural inner join api
        order by category
    """
    rows = read(query, fields=['name', 'author', 'category'])
    return { "data" : rows }